
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example-component', require('./components/ExampleComponent.vue'));
Vue.component('header-component', require('./components/TheHeader.vue'));
Vue.component('footer-component', require('./components/TheFooter.vue'));
Vue.component('landing-page', require('./components/LandingPage.vue'));
Vue.component('search-page', require('./components/SearchPage.vue'));
Vue.component('about-page', require('./components/AboutPage.vue'));
Vue.component('calc-page', require('./components/CalcPage.vue'));
Vue.component('news-page', require('./components/NewsPage.vue'));
Vue.component('reviews-page', require('./components/ReviewsPage.vue'));

import VueProgressiveImage from 'vue-progressive-image'

Vue.use(VueProgressiveImage)

import { library } from '@fortawesome/fontawesome-svg-core'
import { faSearch, faCommentAlt, faUserAlt, faCommentDots, faCalculator, faCarSide, faMotorcycle, faShuttleVan, faTruck, faTrain, faThumbsDown, faThumbsUp, faCheck } from '@fortawesome/free-solid-svg-icons'
import { faUser, faEye, faNewspaper } from '@fortawesome/free-regular-svg-icons'
import { faInstagram, faFacebookF, faGooglePlusG, faTwitter } from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import VueAwesomeSwiper from 'vue-awesome-swiper'
import 'swiper/dist/css/swiper.css'

library.add(faSearch, faCommentAlt, faUserAlt, faCommentDots, faCalculator, faCarSide, faMotorcycle, faShuttleVan, faTruck, faTrain, faThumbsDown, faThumbsUp, faCheck )
library.add( faUser, faEye, faNewspaper )
library.add( faInstagram, faFacebookF, faGooglePlusG, faTwitter )
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(VueAwesomeSwiper, /* { default global options } */ )

Vue.config.productionTip = true

const app = new Vue({
    el: '#app'
});
