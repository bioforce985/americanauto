<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laraveldaily\Quickadmin\Observers\UserActionsObserver;


use Illuminate\Database\Eloquent\SoftDeletes;

class Model extends Model {

    use SoftDeletes;

    /**
    * The attributes that should be mutated to dates.
    *
    * @var array
    */
    protected $dates = ['deleted_at'];

    protected $table    = 'model';
    
    protected $fillable = [
          'name',
          'make_id'
    ];
    

    public static function boot()
    {
        parent::boot();

        Model::observe(new UserActionsObserver);
    }
    
    public function make()
    {
        return $this->hasOne('App\Make', 'id', 'make_id');
    }


    
    
    
}