<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Model;
use App\Http\Requests\CreateModelRequest;
use App\Http\Requests\UpdateModelRequest;
use Illuminate\Http\Request;

use App\Make;


class ModelController extends Controller {

	/**
	 * Display a listing of model
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $model = Model::with("make")->get();

		return view('admin.model.index', compact('model'));
	}

	/**
	 * Show the form for creating a new model
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    $make = Make::pluck("id", "id")->prepend('Please select', 0);

	    
	    return view('admin.model.create', compact("make"));
	}

	/**
	 * Store a newly created model in storage.
	 *
     * @param CreateModelRequest|Request $request
	 */
	public function store(CreateModelRequest $request)
	{
	    
		Model::create($request->all());

		return redirect()->route(config('quickadmin.route').'.model.index');
	}

	/**
	 * Show the form for editing the specified model.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$model = Model::find($id);
	    $make = Make::pluck("id", "id")->prepend('Please select', 0);

	    
		return view('admin.model.edit', compact('model', "make"));
	}

	/**
	 * Update the specified model in storage.
     * @param UpdateModelRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateModelRequest $request)
	{
		$model = Model::findOrFail($id);

        

		$model->update($request->all());

		return redirect()->route(config('quickadmin.route').'.model.index');
	}

	/**
	 * Remove the specified model from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Model::destroy($id);

		return redirect()->route(config('quickadmin.route').'.model.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Model::destroy($toDelete);
        } else {
            Model::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.model.index');
    }

}
