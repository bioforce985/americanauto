<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Redirect;
use Schema;
use App\Make;
use App\Http\Requests\CreateMakeRequest;
use App\Http\Requests\UpdateMakeRequest;
use Illuminate\Http\Request;



class MakeController extends Controller {

	/**
	 * Display a listing of make
	 *
     * @param Request $request
     *
     * @return \Illuminate\View\View
	 */
	public function index(Request $request)
    {
        $make = Make::all();

		return view('admin.make.index', compact('make'));
	}

	/**
	 * Show the form for creating a new make
	 *
     * @return \Illuminate\View\View
	 */
	public function create()
	{
	    
	    
	    return view('admin.make.create');
	}

	/**
	 * Store a newly created make in storage.
	 *
     * @param CreateMakeRequest|Request $request
	 */
	public function store(CreateMakeRequest $request)
	{
	    
		Make::create($request->all());

		return redirect()->route(config('quickadmin.route').'.make.index');
	}

	/**
	 * Show the form for editing the specified make.
	 *
	 * @param  int  $id
     * @return \Illuminate\View\View
	 */
	public function edit($id)
	{
		$make = Make::find($id);
	    
	    
		return view('admin.make.edit', compact('make'));
	}

	/**
	 * Update the specified make in storage.
     * @param UpdateMakeRequest|Request $request
     *
	 * @param  int  $id
	 */
	public function update($id, UpdateMakeRequest $request)
	{
		$make = Make::findOrFail($id);

        

		$make->update($request->all());

		return redirect()->route(config('quickadmin.route').'.make.index');
	}

	/**
	 * Remove the specified make from storage.
	 *
	 * @param  int  $id
	 */
	public function destroy($id)
	{
		Make::destroy($id);

		return redirect()->route(config('quickadmin.route').'.make.index');
	}

    /**
     * Mass delete function from index page
     * @param Request $request
     *
     * @return mixed
     */
    public function massDelete(Request $request)
    {
        if ($request->get('toDelete') != 'mass') {
            $toDelete = json_decode($request->get('toDelete'));
            Make::destroy($toDelete);
        } else {
            Make::whereNotNull('id')->delete();
        }

        return redirect()->route(config('quickadmin.route').'.make.index');
    }

}
